﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Vuforia;
using OpenCVForUnity;

/// <summary>
/// Camera image to mat sample.
/// https://developer.vuforia.com/library/articles/Solution/How-To-Access-the-Camera-Image-in-Unity
/// </summary>
public class CameraImageToMatExample : MonoBehaviour
{


	//private Image.PIXEL_FORMAT m_PixelFormat = Image.PIXEL_FORMAT.RGBA8888;
	private Image.PIXEL_FORMAT m_PixelFormat = Image.PIXEL_FORMAT.RGB888;
    private bool m_RegisteredFormat = false;
    public GameObject quad;
    public Camera mainCamera;
    Mat inputMat;
	Mat thresholdMat;
	Mat rgbMat;
	Mat hsvMat;
    Texture2D outputTexture;

	const int MAX_NUM_OBJECTS = 50;
	const int MIN_OBJECT_AREA = 5;//20 * 20;

	public Scalar orangeLower = new Scalar (0, 199, 219);
	public Scalar orangeUpper = new Scalar (27, 240, 250);

	public GameObject indicatorObject;

	public Vector2 indicatorPos;

	public UnityEngine.UI.Text text;

	public Transform topLeftTrans;

	public Color topLeftColour;

	public Vector2 indicc;

	void LoadFromPlayerPrefs(){

		orangeLower.val [0] = 0;
		orangeLower.val [1] = 155;
		orangeLower.val [2] = 127;
		orangeLower.val [3] = 0;

		orangeUpper.val [0] = 200;
		orangeUpper.val [1] = 227;
		orangeUpper.val [2] = 255;
		orangeUpper.val [3] = 0;


		#if UNITY_EDITOR
		orangeLower.val [0] = (double)PlayerPrefs.GetFloat ("L0");
		orangeLower.val [1] = (double)PlayerPrefs.GetFloat ("L1");
		orangeLower.val [2] = (double)PlayerPrefs.GetFloat ("L2");
		orangeLower.val [3] = (double)PlayerPrefs.GetFloat ("L3");

		orangeUpper.val [0] = (double)PlayerPrefs.GetFloat ("U0");
		orangeUpper.val [1] = (double)PlayerPrefs.GetFloat ("U1");
		orangeUpper.val [2] = (double)PlayerPrefs.GetFloat ("U2");
		orangeUpper.val [3] = (double)PlayerPrefs.GetFloat ("U3");
		#endif

		Debug.Log (orangeLower);
		Debug.Log ("Values loaded from prefs");

	}

    void Start ()
    {
		#if UNITY_EDITOR
			m_PixelFormat = Image.PIXEL_FORMAT.RGBA8888;
		#endif

		try{
			LoadFromPlayerPrefs ();
		} catch (Exception e) {
			Debug.LogException(e, this);
		}

        VuforiaBehaviour qcarBehaviour = (VuforiaBehaviour)FindObjectOfType (typeof(VuforiaBehaviour));
        if (qcarBehaviour) {
            qcarBehaviour.UpdateEvent += OnTrackablesUpdated;
        }
		thresholdMat = new Mat ();
		hsvMat = new Mat ();
    }

	int ticker = 0;

    public void OnTrackablesUpdated ()
    {
		ticker++;

		if (ticker % 50 != 0) {
			return;
		}

        if (!m_RegisteredFormat) {
            CameraDevice.Instance.SetFrameFormat (m_PixelFormat, true);
            m_RegisteredFormat = true;
        }
		
        CameraDevice cam = CameraDevice.Instance;
        Image image = cam.GetCameraImage (m_PixelFormat);
        if (image == null) {
            Debug.Log (m_PixelFormat + " image is not available yet");
        } else {
			
            if (inputMat == null) {
				inputMat = new Mat (image.Height, image.Width, CvType.CV_8UC4);
                //inputMat = new Mat (image.Height, image.Width, CvType.CV_8UC1);
				rgbMat = new Mat (image.Height, image.Width, CvType.CV_8UC3);
                //Debug.Log ("inputMat dst ToString " + inputMat.ToString ());
            }

			//inputMat.put (image.Width, image.Height, image.Pixels);
            inputMat.put (0, 0, image.Pixels);

            Imgproc.putText (inputMat, "CameraImageToMatSample " + inputMat.cols () + "x" + inputMat.rows (), new Point (5, inputMat.rows () - 5), Core.FONT_HERSHEY_PLAIN, 1.0, new Scalar (255, 0, 0, 255));

			//Imgproc.circle (inputMat, new Point (indicatorPos.x, indicatorPos.y), 20, new Scalar(255, 255, 255), 10);

			//Imgproc.cvtColor (inputMat, rgbMat, Imgproc.COLOR_RGBA2RGB);
			Imgproc.cvtColor (rgbMat, hsvMat, Imgproc.COLOR_RGB2HSV);

			Core.inRange (hsvMat, orangeLower, orangeUpper, thresholdMat);

			trackPendulum (thresholdMat, inputMat, image);

			//Ray ray = mainCamera.ScreenPointToRay(new Vector3((indicatorPos.x / image.Width) * mainCamera.scaledPixelWidth, (indicatorPos.y / image.Height) * mainCamera.scaledPixelHeight, 0));
			//Debug.DrawRay(mainCamera.transform.position, ray.direction * 10, Color.yellow);

            if (outputTexture == null) {
                outputTexture = new Texture2D (inputMat.cols (), inputMat.rows (), TextureFormat.RGBA32, false);
            }
			
			Utils.matToTexture2D (inputMat, outputTexture);

			Vector3 screenPoint = Camera.main.WorldToScreenPoint (topLeftTrans.position);

			indicc = new Vector2 (screenPoint.x, screenPoint.y);

			topLeftColour = outputTexture.GetPixel ((int)screenPoint.x, (int)screenPoint.y);

			//topLeftColour = outputTexture.GetPixel(outpu

			//topLeftColour = outputTexture.GetPixel (outputTexture.width/2, outputTexture.height/2);

			//quad.transform.localScale = new Vector3 ((float)image.Width, mainCamera.scaledPixelHeight, 1.0f);
            quad.transform.localScale = new Vector3 ((float)image.Width*0.1f, (float)image.Height*0.1f, 1.0f);

			Texture2D testTex = new Texture2D(thresholdMat.cols (), thresholdMat.rows (), TextureFormat.RGBA32, false);

			Utils.matToTexture2D (thresholdMat, testTex);

			quad.GetComponent<Renderer> ().material.mainTexture = testTex;//outputTexture;

            //mainCamera.orthographicSize = image.Height / 2;
			
        }
    }

	public Color[] GetImage(){
		return outputTexture.GetPixels();
	}

	void trackPendulum(Mat threshold, Mat cameraFeed, Image image){

		Mat temp = new Mat ();
		threshold.copyTo (temp);

		List<MatOfPoint> contours = new List<MatOfPoint> ();

		Mat hierarchy = new Mat ();
		//find contours of filtered image using openCV findContours function
		Imgproc.findContours (temp, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

		//hierarchy = threshold;

		Debug.Log ("Tracking pendulum...");

		text.text = "tracking pendulum " + hierarchy.rows();


		if (hierarchy.rows () > 0) {

			int numObjects = hierarchy.rows ();

			Debug.Log ("num rows... " + numObjects);

			if (numObjects < MAX_NUM_OBJECTS) {

				double maxArea = 0;
				int xPos = 0;
				int yPos = 0;

				for (int index = 0; index >= 0; index = (int)hierarchy.get (0, index) [0]) {

					Moments moment = Imgproc.moments (contours [index]);
					double area = moment.get_m00 ();

					if (area > MIN_OBJECT_AREA && area > maxArea) {

						maxArea = area;

						xPos = (int)(moment.get_m10 () / area);
						yPos = (int)(moment.get_m01 () / area);

					}

				}

				if (maxArea > 0) {

					text.text = xPos + " | " + yPos;

					Debug.Log ("x pos: " + xPos + " ... y pos: " + yPos);

					/*Ray ray;

					try{
						ray = mainCamera.ScreenPointToRay(Input.GetTouch(0).position);
					} catch (Exception e) {
						ray = mainCamera.ScreenPointToRay(Input.mousePosition);
					}*/

					Ray ray = mainCamera.ScreenPointToRay(new Vector3(convertToCamWidth(xPos, image), convertToCamHeight(yPos, image), 0));

					//Ray ray = mainCamera.ScreenPointToRay(new Vector3((indicatorPos.x / cameraFeed.width()) * mainCamera.scaledPixelWidth, (indicatorPos.y / cameraFeed.height()) * mainCamera.scaledPixelHeight, 0));
					//Ray ray = mainCamera.ScreenPointToRay(new Vector3((float)((moment.get_m10 () / area) / cameraFeed.width()) * mainCamera.scaledPixelWidth, (float)((moment.get_m01 () / area) / cameraFeed.height()) * mainCamera.scaledPixelHeight, 0));
					Debug.DrawRay(mainCamera.transform.position, ray.direction * 1000, Color.yellow);

					RaycastHit hit;

					if (Physics.Raycast (ray, out hit, 1000f, 1<<8)) {

						indicatorObject.transform.position = hit.point;
					} 

					//Imgproc.circle (cameraFeed, new Point ((int)(moment.get_m10 () / maxArea), (int)(moment.get_m01 () / maxArea)), 50, new Scalar (255, 255, 255, 255), 5);

				}

			} else {

				Imgproc.putText (cameraFeed, "Filter too noisy!", new Point (5, cameraFeed.rows () - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar (255, 255, 255, 255), 2, Imgproc.LINE_AA, false);

			}

		}

	}

	float convertToCamWidth(float xPos, Image image) {
		return (xPos / image.Width) * mainCamera.scaledPixelWidth;
	}

	float convertToCamHeight(float yPos, Image image) {
		return ((image.Height-yPos) / image.Height) * mainCamera.scaledPixelHeight;
	}
}
