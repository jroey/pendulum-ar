﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LengthIndicatorPos : MonoBehaviour {

	public Transform baseTrans;
	public Transform indicatorTrans;

	public float xDisplacement;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		//transform.position = (indicatorTrans.position - baseTrans.position).normalized * Vector3.Distance (indicatorTrans.position, baseTrans.position) ;

		//transform.position = new Vector3 (transform.position.x, transform.position.y, baseTrans.position.z);

		transform.position = new Vector3 ((indicatorTrans.position.x + baseTrans.position.x) / 2 + xDisplacement, (indicatorTrans.position.y + baseTrans.position.y) / 2, (indicatorTrans.position.z + baseTrans.position.z) / 2);

	}
}
