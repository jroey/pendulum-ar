﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCamera : MonoBehaviour {

	public Vector3 rotation;
	public Vector3 position;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.rotation = Quaternion.Euler (rotation);
		transform.position = position;

	}
}
