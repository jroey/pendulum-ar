﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorAugmentation : MonoBehaviour {

	public GameObject bobGameObject;

	public Transform botTransform;

	public VectorArrow realVelArrow;
	public VectorArrow realAccArrow;
	public VectorArrow stringArrow;

	public Vector3 prevPos;

	public Vector3 prevVel;

	private List<Vector3> velocityVectors;
	public int numVelAverages;
	private int currentVelIndex;

	private List<Vector3> accelerationVectors;
	public int numAccAverages;
	private int currentAccIndex;

	// Use this for initialization
	void Start () {

		velocityVectors = new List<Vector3> (numVelAverages);

		for (int i = 0; i < numVelAverages; i++) {

			velocityVectors.Add (Vector3.zero);

		}

		accelerationVectors = new List<Vector3> (numAccAverages);

		for (int i = 0; i < numAccAverages; i++) {

			accelerationVectors.Add (Vector3.zero);

		}

	}

	public void ChangeVelAvgs(float numAverages) {

		numVelAverages = (int)numAverages;

		velocityVectors = new List<Vector3> (numVelAverages);

		for (int i = 0; i < numVelAverages; i++) {

			velocityVectors.Add (Vector3.zero);

		}

		currentVelIndex = 0;

	}

	public void ChangeAccAvgs(float numAverages) {

		numAccAverages = (int)numAverages;

		accelerationVectors = new List<Vector3> (numAccAverages);

		for (int i = 0; i < numAccAverages; i++) {

			accelerationVectors.Add (Vector3.zero);

		}

		currentAccIndex = 0;

	}

	Vector3 GetAverageVelVector() {

		Vector3 averageVector = Vector3.zero;

		for (int i = 0; i < numVelAverages; i++) {

			averageVector += velocityVectors[i];

		}

		averageVector /= numVelAverages;


		return averageVector;
	}

	Vector3 GetAverageAccVector() {

		Vector3 averageVector = Vector3.zero;

		for (int i = 0; i < numAccAverages; i++) {

			averageVector += accelerationVectors[i];

		}

		averageVector /= numAccAverages;


		return averageVector;
	}

	void AddVelVecToAverage(Vector3 velVec) {

		velocityVectors [currentVelIndex] = velVec;

	}

	void AddAccVecToAverage(Vector3 accVec) {

		accelerationVectors [currentAccIndex] = accVec;

	}

	// Update is called once per frame
	void Update () {

		Vector3 velVec = bobGameObject.transform.position - prevPos;

		if ((velVec.x < 0 && prevVel.x >= 0) || (velVec.x > 0 && prevVel.x <= 0)) {
			TickerTape.DirectionReversed ();
		}

		if (RecordingManager.isLooping) {
			return;
		}

		AddVelVecToAverage (velVec);

		if (numVelAverages != 1)
			velVec = GetAverageVelVector ();

		realVelArrow.vector = velVec;

		Vector3 accVec = velVec - prevVel;

		AddAccVecToAverage (accVec);

		if (numAccAverages != 1)
			accVec = GetAverageAccVector ();

		realAccArrow.vector = accVec;

		prevPos = bobGameObject.transform.position;

		prevVel = velVec;

		if (stringArrow) {

			Vector3 stringVec = (botTransform.position - bobGameObject.transform.position).normalized;

			stringArrow.vector = stringVec;

		}
		// Increment vel/acc index

		currentVelIndex++;

		if (currentVelIndex == numVelAverages) {
			currentVelIndex = 0;
		}

		currentAccIndex++;

		if (currentAccIndex == numAccAverages) {
			currentAccIndex = 0;
		}

	}

}
