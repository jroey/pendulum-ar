﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

	public GameObject motionVectorPanel;
	public GameObject forceVectorPanel;
	public GameObject generalInfoPanel;
	public GameObject sinusoidalPanel;
	public GameObject dotsPanel;

	public GameObject playbackBackButton;
	public GameObject playbackSlowerButton;
	public GameObject playbackFasterButton;
	public GameObject playbackScrobbler;
	public Text playbackSpeedText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GotoPendulum() {

		SceneManager.LoadScene("iOS", LoadSceneMode.Single);

	}

	public void GotoCalibrate() {

		SceneManager.LoadScene("Calibration", LoadSceneMode.Single);

	}

	public void GotoAbout() {

		SceneManager.LoadScene("About", LoadSceneMode.Single);

	}

	public void EnterPlaybackMode() {
		
		playbackBackButton.SetActive (true);
		playbackSlowerButton.SetActive (true);
		playbackFasterButton.SetActive (true);
		playbackScrobbler.SetActive (true);
		playbackSpeedText.gameObject.SetActive (true);

	}

	public void ExitPlaybackMode() {

		playbackBackButton.SetActive (false);
		playbackSlowerButton.SetActive (false);
		playbackFasterButton.SetActive (false);
		playbackScrobbler.SetActive (false);
		playbackSpeedText.gameObject.SetActive (false);

	}

	private void hideAllPanels() {

		motionVectorPanel.SetActive (false);
		forceVectorPanel.SetActive (false);
		generalInfoPanel.SetActive (false);
		sinusoidalPanel.SetActive (false);
		dotsPanel.SetActive (false);

	}

	public void ChangeDataScreen(int selectionIndex) {

		hideAllPanels ();

		if (selectionIndex == 1) {
			generalInfoPanel.SetActive (true);
		} else if (selectionIndex == 2) {
			dotsPanel.SetActive (true);
		} else if (selectionIndex == 3) {
			forceVectorPanel.SetActive (true);
		} else if (selectionIndex == 4) {
			motionVectorPanel.SetActive (true);
		} else if (selectionIndex == 5) {
			sinusoidalPanel.SetActive (true);
		} 

	}

	public void ReturnToMenu() {

		SceneManager.LoadScene("Menu", LoadSceneMode.Single);

	}

}