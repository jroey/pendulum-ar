﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawVectors : MonoBehaviour {

	public GameObject TargPend;
	public GameObject TargBase;
	public GameObject ArrowBase;
	public GameObject ArrowBasePred;

	public GameObject AccelerationActual;

	static float lastangle = 0;    // angle from most recent update
	static float recentangle = 0;  // angle from last time direction was updated
	static bool direction = true;
	static float[] speeds;
	static int speedi, speedn;

	static float maxangle = 0;

	bool velVecEnabled = false;

	public GameObject vecIndicatorGameobject;

	public TickerTape tickerTape;

	private float prevSpeed;

	public Vector3 velVec;
	public Vector3 accVec;

	public GameObject randomIndicator;

	public float scalerrr;

	// Use this for initialization
	void Start () {
		/*TargPend = GameObject.Find("TargPend");
		TargBase = GameObject.Find("TargBase");
		ArrowBase = GameObject.Find("ArrowBase");
		ArrowBasePred = GameObject.Find("ArrowBasePred");*/

		speedn = 10;
		speeds = new float[speedn];
		for (speedi = 0; speedi < speedn; speedi++)
		{
			print("TESTING : " + speedi);
			speeds[speedi] = 0f;
		}
		speedi = 0;
	}

	private Vector3 prevAcc;

	// Update is called once per frame
	void Update () {

		if (RecordingManager.isLooping) {
			return;
		}

		/// determine the angle of the real pendulum based on the target
		float basex = TargBase.transform.position.y;
		float pendx = TargPend.transform.position.y;
		float basey = TargBase.transform.position.z;
		float pendy = TargPend.transform.position.z;
		float angle = Mathf.Atan2(basex - pendx, basey + 4 - pendy) * Mathf.Rad2Deg;
		if (Mathf.Abs(angle) > maxangle) maxangle = Mathf.Abs(angle);

		// move the virtual pendulum to the same angle as the real pendulum
		transform.localEulerAngles = new Vector3(0, angle, 0);

		// determine the predicted speed if this is a proper pendulum
		float speedfactor = Mathf.Cos(Mathf.Abs(angle) * Mathf.Deg2Rad) - Mathf.Cos(maxangle * Mathf.Deg2Rad);
		float speedpred = Mathf.Sqrt(2.0f * 9.81f * 4 * speedfactor);
		if (Mathf.Abs(angle - recentangle) > 1)
		{
			if (angle > recentangle) direction = false; else direction=true;
			recentangle = angle;
		}
		if (direction) speedpred = -speedpred;

		// And determine the actual pendulum speed (and rolling average)
		float arcdistance = 4 * (angle - lastangle) * Mathf.Deg2Rad;
		float speed = arcdistance / Time.deltaTime;
		speeds[speedi] = speed;
		speedi = (speedi + 1) % speedn;

		int i;
		float speedave = 0;
		for (i = 0; i<speedn; i++) speedave += speeds[i];
		speedave = speedave / speedn;
		//print("time=" + Time.deltaTime + "   speedave=" + speedave + "   speed=" + speed + "  speedave=" + speedave + "   angle=" + angle + " maxangle=" + maxangle + "  speedpred=" + speedpred + "  dir=" + direction);

		/*if (ArrowBase.transform.position.y > 0) {
			speedave = -speedave;
		}*/

		if ((prevSpeed < 0 && speedave > 0) || (prevSpeed > 0 && speedave < 0)) {

			TickerTape.DirectionReversed ();
			Debug.Log ("reversal");

		}
		prevSpeed = speedave;

		// And scale the velocity arrows
		ArrowBase.transform.localScale = new Vector3(1f, -speedave * 5f,1f);
		ArrowBasePred.transform.localScale = new Vector3(1f, speedpred * 5f, 1f);

		//Vector3 arrowHeadPos = ArrowBase.transform.position + 0.1f * speedave * Vector3.Cross (ArrowBase.transform.up, (ArrowBase.transform.position - TargBase.transform.position).normalized);

		//arrowHeadPos.y = 0;

		//vecIndicatorGameobject.transform.position = Vector3.Cross (ArrowBase.transform.right, (ArrowBase.transform.position - TargBase.transform.position).normalized);
		//vecIndicatorGameobject.transform.position = arrowHeadPos;

		ArrowBase.transform.rotation = Quaternion.LookRotation (ArrowBase.transform.position - TargBase.transform.position);
		//ArrowBase.transform.rotation = Quaternion.LookRotation (ArrowBase.transform.position - TargBase.transform.position);
		ArrowBasePred.transform.rotation = Quaternion.LookRotation (ArrowBasePred.transform.position - TargBase.transform.position);

		Vector3 vecc = ArrowBase.transform.up;

		//velVec = new Vector3 (vecc.y, vecc.x, 0);

		if (ArrowBase.transform.localScale.y < 0) {
			vecc.y = -vecc.y;
			vecc.x = -vecc.x;
		}
			
		Debug.Log (vecc);

		//velVec = new Vector3 (vecc.y, vecc.x, 0);

		Vector3 acccc = prevAcc - vecc;

		//vecc = new Vector3 (vecc.x, vecc.z, vecc.y);

		//acccc = new Vector3 (acccc.y, acccc.x, acccc.z);

		//randomIndicator.transform.position = ArrowBase.transform.position + vecc * scalerrr;
		randomIndicator.transform.position = ArrowBase.transform.position + acccc;

		prevAcc = vecc;

		//Vector3 scaleVec = speedave * 20f * Vector3.Cross (ArrowBase.transform.right, (ArrowBase.transform.position - TargBase.transform.position).normalized);

		//scaleVec.x = scaleVec.y;

		//scaleVec.y = 0.5f;

		//ArrowBase.transform.localScale = scaleVec;



		// And remember the angle for next time round this loop
		lastangle = angle;
	}

	public void ToggleVelVec (bool velVecEnabled){
		this.velVecEnabled = velVecEnabled;
		ArrowBase.SetActive (velVecEnabled);
	}

}

/*public class DrawVectors : MonoBehaviour {

	public LineRenderer velocityLineRenderer;

	public Transform baseTrans;

	public float maxDisplacement; // maximum distance the pendulum weight gets from the origin

	public float sampleTime;

	public float maxVectorSize;

	public bool isSampling = true;

	private float startTime;

	private bool velVecEnabled = true;

	public TextMesh thetaTextMesh;


	// Use this for initialization
	void Start () {

		startTime = Time.time;

	}

	private float prevX;
	private bool movingRight = false;


	// Update is called once per frame
	void Update () {

		if (isSampling && Time.time - startTime >= sampleTime) {
			isSampling = false;
		} else if (isSampling) {

			if (Mathf.Abs (transform.position.x) > maxDisplacement) {
			
				maxDisplacement = Mathf.Abs (transform.position.x);

			}

		} else {

			if (Mathf.Abs (transform.position.x) > maxDisplacement) {

				maxDisplacement = Mathf.Abs (transform.position.x);

			}

			// We are drawing

			float vectorSize = (maxDisplacement-(transform.position.x/maxDisplacement))*maxVectorSize;

			Debug.Log (vectorSize);

			Vector3 velPos = Vector3.Cross (Vector3.up, (transform.position - baseTrans.position).normalized);

			Vector3 velVec = transform.position + (transform.position - velPos).normalized * vectorSize;

			//velocityLineRenderer.SetPosition (0, transform.position);
			//velocityLineRenderer.SetPosition (1, velVec);


			Vector3 targetDir = baseTrans.position - transform.position;

			if (transform.position.x > prevX) {
				movingRight = true;
			} else if (transform.position.x < prevX) {
				movingRight = false;
			}
				
			float angle = Vector3.Angle(targetDir, baseTrans.forward);

			//Vector3 velPosMath = transform.position + Vector3(0, Mathf

			float radAngle = Mathf.Deg2Rad * angle;

			/*if (!movingRight) {
				radAngle = -radAngle;
			}/

			float maxVecSize = 0.1f;

			float displacementRatio = (transform.position.x / maxDisplacement);

			Debug.Log ("Mathf.Cos (radAngle): " + Mathf.Cos (radAngle));
			Debug.Log ("Displacement Ratio: " + displacementRatio);

			velocityLineRenderer.SetPosition (0, transform.position);
			velocityLineRenderer.SetPosition (1, transform.position + new Vector3 (Mathf.Cos (radAngle), 0, 0));

			velocityLineRenderer.enabled = velVecEnabled;

			if (velVecEnabled) {
				thetaTextMesh.text = Mathf.Cos (radAngle) + " " + movingRight.ToString ();
				//thetaTextMesh.text = "Theta " + angle.ToString("F2") + "° " + movingRight.ToString();
			}

			prevX = transform.position.x;

			//velocityLineRenderer.SetPosition (1, Vector3.Cross (transform.right, (transform.position - baseTrans.position).normalized));
			//velocityLineRenderer.SetPosition (1, (transform.position - baseTrans.position).normalized*0.1f);

		}
	}

			public float maxDispAngle;

			// Update is called once per frame
			void Update () {

				Vector3 targetDir = baseTrans.position - transform.position;
				float angle = Vector3.Angle(targetDir, baseTrans.forward) * Mathf.Deg2Rad;

				if (angle > maxDispAngle) {

					maxDispAngle = angle;

				}

				if (isSampling && Time.time - startTime >= sampleTime) {
					isSampling = false;
				} else if (!isSampling) {



					if (transform.position.x > prevX) {
						movingRight = true;
					} else if (transform.position.x < prevX) {
						movingRight = false;
					}
						

					//float velVecX = Mathf.Cos ((Mathf.PI/2) * (angle/maxDispAngle));
					float velVecX = Mathf.Cos (Mathf.Deg2Rad * 90 * (angle/maxDispAngle))*0.1f;

					//float velVecY = Mathf.Sin (Mathf.Deg2Rad * 90 * (angle/maxDispAngle))*0.1f;

					if (!movingRight) {
						velVecX = -velVecX;
					}

					if (velVecEnabled) {
						thetaTextMesh.text = (angle * Mathf.Rad2Deg).ToString() + "°";
					}

					velocityLineRenderer.enabled = velVecEnabled;

					velocityLineRenderer.SetPosition (0, transform.position);
					velocityLineRenderer.SetPosition (1, transform.position + new Vector3 (velVecX, 0, 0));

					prevX = transform.position.x;
				} 

			}

			public void ToggleVelVec (bool velVecEnabled){
				this.velVecEnabled = velVecEnabled;
				thetaTextMesh.gameObject.SetActive (velVecEnabled);
			}/

		
}
*/