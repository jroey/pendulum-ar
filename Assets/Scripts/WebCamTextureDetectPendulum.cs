﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;

namespace OpenCVForUnitySample
{
	/// <summary>
	/// Web cam texture detect circles sample.
	/// </summary>
	[RequireComponent(typeof(WebCamTextureToMatHelper))]
	public class WebCamTextureDetectPendulum : MonoBehaviour
	{

		/// <summary>
		/// The colors.
		/// </summary>
		Color32[] colors;

		/// <summary>
		/// The texture.
		/// </summary>
		Texture2D texture;

		const int MAX_NUM_OBJECTS = 50;

		/// <summary>
		/// minimum and maximum object area
		/// </summary>
		const int MIN_OBJECT_AREA = 20 * 20;

		/// <summary>
		/// The web cam texture to mat helper.
		/// </summary>
		WebCamTextureToMatHelper webCamTextureToMatHelper;

		/// <summary>
		/// The gray mat.
		/// </summary>
		Mat grayMat;

		public Scalar orangeLower = new Scalar (7, 92, 92);
		public Scalar orangeUpper = new Scalar (21, 214, 205);

		Mat rgbMat;
		Mat thresholdMat;
		Mat hsvMat;
		ColorObject green = new ColorObject ("green");

		public bool doMorph = false;
		public bool drawThreshold = false;

		// Use this for initialization
		void Start ()
		{
			webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper> ();
			webCamTextureToMatHelper.Init ();
		}

		/// <summary>
		/// Raises the web cam texture to mat helper inited event.
		/// </summary>
		public void OnWebCamTextureToMatHelperInited ()
		{
			Debug.Log ("OnWebCamTextureToMatHelperInited");

			Mat webCamTextureMat = webCamTextureToMatHelper.GetMat ();

			colors = new Color32[webCamTextureMat.cols () * webCamTextureMat.rows ()];
			texture = new Texture2D (webCamTextureMat.cols (), webCamTextureMat.rows (), TextureFormat.RGBA32, false);

			grayMat = new Mat (webCamTextureMat.rows (), webCamTextureMat.cols (), CvType.CV_8UC1);

			rgbMat = new Mat (webCamTextureMat.rows (), webCamTextureMat.cols (), CvType.CV_8UC3);
			thresholdMat = new Mat ();
			hsvMat = new Mat ();

			gameObject.transform.localScale = new Vector3 (webCamTextureMat.cols (), webCamTextureMat.rows (), 1);

			Debug.Log ("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);

			float width = 0;
			float height = 0;

			width = gameObject.transform.localScale.x;
			height = gameObject.transform.localScale.y;

			float widthScale = (float)Screen.width / width;
			float heightScale = (float)Screen.height / height;
			if (widthScale < heightScale) {
				Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
			} else {
				Camera.main.orthographicSize = height / 2;
			}

			gameObject.GetComponent<Renderer> ().material.mainTexture = texture;

		}

		/// <summary>
		/// Raises the web cam texture to mat helper disposed event.
		/// </summary>
		public void OnWebCamTextureToMatHelperDisposed ()
		{
			Debug.Log ("OnWebCamTextureToMatHelperDisposed");
			if (grayMat != null)
				grayMat.Dispose ();
			if (rgbMat != null)
				rgbMat.Dispose ();
			if (thresholdMat != null)
				thresholdMat.Dispose ();
			if (hsvMat != null)
				hsvMat.Dispose ();

		}

		// Update is called once per frame
		void Update ()
		{

			if (webCamTextureToMatHelper.IsPlaying () && webCamTextureToMatHelper.DidUpdateThisFrame ()) {

				Mat rgbaMat = webCamTextureToMatHelper.GetMat ();								

				//Imgproc.cvtColor (rgbaMat, rgbMat, Imgproc.COLOR_BGR2HSV);
				Imgproc.cvtColor (rgbaMat, rgbMat, Imgproc.COLOR_RGBA2RGB);
				Imgproc.cvtColor (rgbMat, hsvMat, Imgproc.COLOR_RGB2HSV);

				//Core.inRange (hsvMat, green.getHSVmin (), green.getHSVmax (), thresholdMat);
				Core.inRange (hsvMat, orangeLower, orangeUpper, thresholdMat);

				if (doMorph) {
					morphOps (thresholdMat);
				}

				if (!drawThreshold) {
					trackPendulum (thresholdMat, rgbMat);
					Utils.matToTexture2D (rgbMat, texture, colors);
				} else {
					trackPendulum (thresholdMat, thresholdMat);
					Utils.matToTexture2D (thresholdMat, texture, colors);
				}

				//trackFilteredObject (green, thresholdMat, hsvMat, rgbMat);

				//OpenCVForUnity.Core.inRange (rgbaMat, new Scalar (7, 92, 92), new Scalar (21, 214, 205), rgbaMat);
				//OpenCVForUnity.Core.inra				
				//Imgproc.erode (rgbaMat, rgbaMat, null, new Point (-1, -1), 2);
				//Imgproc.erode(grayMat, grayMat, null, 

				/*
					
								Imgproc.cvtColor (rgbaMat, grayMat, Imgproc.COLOR_RGBA2GRAY);

								using (Mat circles = new Mat ()) {
										
										Imgproc.HoughCircles (grayMat, circles, Imgproc.CV_HOUGH_GRADIENT, 2, 10, 160, 50, 10, 20); 
										Point pt = new Point ();
										
										for (int i = 0; i < circles.cols(); i++) {
												double[] data = circles.get (0, i);
												pt.x = data [0];
												pt.y = data [1];
												double rho = data [2];
												Imgproc.circle (rgbaMat, pt, (int)rho, new Scalar (255, 0, 0, 255), 5);
										}
								}

								Imgproc.putText (rgbaMat, "W:" + rgbaMat.width () + " H:" + rgbaMat.height () + " SO:" + Screen.orientation, new Point (5, rgbaMat.rows () - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar (255, 255, 255, 255), 2, Imgproc.LINE_AA, false);
*/

			}

		}

		/// <summary>
		/// Raises the disable event.
		/// </summary>
		void OnDisable ()
		{
			webCamTextureToMatHelper.Dispose ();
		}

		void morphOps (Mat thresh)
		{

			//create structuring element that will be used to "dilate" and "erode" image.
			//the element chosen here is a 3px by 3px rectangle
			Mat erodeElement = Imgproc.getStructuringElement (Imgproc.MORPH_RECT, new Size (3, 3));
			//dilate with larger element so make sure object is nicely visible
			Mat dilateElement = Imgproc.getStructuringElement (Imgproc.MORPH_RECT, new Size (8, 8));

			Imgproc.erode (thresh, thresh, erodeElement);
			Imgproc.erode (thresh, thresh, erodeElement);

			Imgproc.dilate (thresh, thresh, dilateElement);
			Imgproc.dilate (thresh, thresh, dilateElement);
		}

		void drawObject (List<ColorObject> theColorObjects, Mat frame, Mat temp, List<MatOfPoint> contours, Mat hierarchy)
		{

			for (int i = 0; i < theColorObjects.Count; i++) {
				Imgproc.drawContours (frame, contours, i, theColorObjects [i].getColor (), 3, 8, hierarchy, int.MaxValue, new Point ());
				Imgproc.circle (frame, new Point (theColorObjects [i].getXPos (), theColorObjects [i].getYPos ()), 5, theColorObjects [i].getColor ());
				Imgproc.putText (frame, theColorObjects [i].getXPos () + " , " + theColorObjects [i].getYPos (), new Point (theColorObjects [i].getXPos (), theColorObjects [i].getYPos () + 20), 1, 1, theColorObjects [i].getColor (), 2);
				Imgproc.putText (frame, theColorObjects [i].getType (), new Point (theColorObjects [i].getXPos (), theColorObjects [i].getYPos () - 20), 1, 2, theColorObjects [i].getColor (), 2);
			}
		}

		public void Next() {

			SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);

		}

		public void Calibrate(){
			PlayerPrefs.SetFloat ("L0", (float)orangeLower.val[0]);
			PlayerPrefs.SetFloat ("L1", (float)orangeLower.val[1]);
			PlayerPrefs.SetFloat ("L2", (float)orangeLower.val[2]);
			PlayerPrefs.SetFloat ("L3", (float)orangeLower.val[3]);

			PlayerPrefs.SetFloat ("U0", (float)orangeUpper.val[0]);
			PlayerPrefs.SetFloat ("U1", (float)orangeUpper.val[1]);
			PlayerPrefs.SetFloat ("U2", (float)orangeUpper.val[2]);
			PlayerPrefs.SetFloat ("U3", (float)orangeUpper.val[3]);

			SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);

		}

		void trackPendulum(Mat threshold, Mat cameraFeed){

			Mat temp = new Mat ();
			threshold.copyTo (temp);

			List<MatOfPoint> contours = new List<MatOfPoint> ();

			Mat hierarchy = new Mat ();
			//find contours of filtered image using openCV findContours function
			Imgproc.findContours (temp, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

			if (hierarchy.rows () > 0) {

				int numObjects = hierarchy.rows ();

				if (numObjects < MAX_NUM_OBJECTS) {

					double maxArea = 0;
					int xPos = 0;
					int yPos = 0;

					for (int index = 0; index >= 0; index = (int)hierarchy.get (0, index) [0]) {

						Moments moment = Imgproc.moments (contours [index]);
						double area = moment.get_m00 ();

						if (area > MIN_OBJECT_AREA && area > maxArea) {

							maxArea = area;


							xPos = (int)(moment.get_m10 () / area);
							yPos = (int)(moment.get_m01 () / area);

						}

					}

					if (maxArea > 0) {
						ColorObject colorObject = new ColorObject ();
						colorObject.setXPos (xPos);
						colorObject.setYPos (yPos);

						Imgproc.circle (cameraFeed, new Point (xPos, yPos), 50, orangeLower, 5);

					}
				} else {

					Imgproc.putText (cameraFeed, "Filter too noisy!", new Point (5, cameraFeed.rows () - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar (255, 255, 255, 255), 2, Imgproc.LINE_AA, false);

				}

			}

		}

		void trackFilteredObject (ColorObject theColorObject, Mat threshold, Mat HSV, Mat cameraFeed)
		{

			List<ColorObject> colorObjects = new List<ColorObject> ();
			Mat temp = new Mat ();
			threshold.copyTo (temp);
			//these two vectors needed for output of findContours
			List<MatOfPoint> contours = new List<MatOfPoint> ();
			Mat hierarchy = new Mat ();
			//find contours of filtered image using openCV findContours function
			Imgproc.findContours (temp, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

			//use moments method to find our filtered object
			bool colorObjectFound = false;
			if (hierarchy.rows () > 0) {
				int numObjects = hierarchy.rows ();

				//						Debug.Log("hierarchy " + hierarchy.ToString());

				//if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
				if (numObjects < MAX_NUM_OBJECTS) {
					for (int index = 0; index >= 0; index = (int)hierarchy.get(0, index)[0]) {

						Moments moment = Imgproc.moments (contours [index]);
						double area = moment.get_m00 ();

						//if the area is less than 20 px by 20px then it is probably just noise
						//if the area is the same as the 3/2 of the image size, probably just a bad filter
						//we only want the object with the largest area so we safe a reference area each
						//iteration and compare it to the area in the next iteration.
						if (area > MIN_OBJECT_AREA) {

							ColorObject colorObject = new ColorObject ();

							colorObject.setXPos ((int)(moment.get_m10 () / area));
							colorObject.setYPos ((int)(moment.get_m01 () / area));
							colorObject.setType (theColorObject.getType ());
							colorObject.setColor (theColorObject.getColor ());

							colorObjects.Add (colorObject);

							colorObjectFound = true;

						} else {
							colorObjectFound = false;
						}
					}
					//let user know you found an object
					if (colorObjectFound == true) {
						//draw object location on screen
						drawObject (colorObjects, cameraFeed, temp, contours, hierarchy);
					}

				} else {
					Imgproc.putText (cameraFeed, "TOO MUCH NOISE!", new Point (5, cameraFeed.rows () - 10), Core.FONT_HERSHEY_SIMPLEX, 1.0, new Scalar (255, 255, 255, 255), 2, Imgproc.LINE_AA, false);
				}
			}
		}

		/// <summary>
		/// Raises the back button event.
		/// </summary>
		public void OnBackButton ()
		{
			#if UNITY_5_3 || UNITY_5_3_OR_NEWER
			SceneManager.LoadScene ("OpenCVForUnitySample");
			#else
			Application.LoadLevel ("OpenCVForUnitySample");
			#endif
		}

		public void ChangeL1(float val){
			orangeLower.set (new double[]{val, orangeLower.val [1], orangeLower.val [2]});
		}

		public void ChangeL2(float val){
			orangeLower.set (new double[]{orangeLower.val [0], val, orangeLower.val [2]});
		}

		public void ChangeL3(float val){
			orangeLower.set (new double[]{orangeLower.val [0], orangeLower.val [1], val});
		}

		public void ChangeU1(float val){
			orangeUpper.set (new double[]{val, orangeUpper.val [1], orangeUpper.val [2]});
		}

		public void ChangeU2(float val){
			orangeUpper.set (new double[]{orangeUpper.val [0], val, orangeUpper.val [2]});
		}

		public void ChangeU3(float val){
			orangeUpper.set (new double[]{orangeUpper.val [0], orangeUpper.val [1], val});
		}

		public void ToggleViews(){

			drawThreshold = !drawThreshold;

		}

		public void ChangeScalarValue(bool isLower, int index, double value){

			if (isLower) {

				double[] newVals = new double[orangeLower.val.Length];

				for (int i = 0; i < newVals.Length; i++) {
				
					if (i == index) {
						newVals [i] = value;
					} else {
						newVals [i] = orangeLower.val [i];
					}
				
				}

				orangeLower.set (newVals);
			} else {
				double[] newVals = new double[orangeUpper.val.Length];

				for (int i = 0; i < newVals.Length; i++) {

					if (i == index) {
						newVals [i] = value;
					} else {
						newVals [i] = orangeUpper.val [i];
					}

				}

				orangeUpper.set (newVals);
			}


		}

		/// <summary>
		/// Raises the play button event.
		/// </summary>
		public void OnPlayButton ()
		{
			webCamTextureToMatHelper.Play ();
		}

		/// <summary>
		/// Raises the pause button event.
		/// </summary>
		public void OnPauseButton ()
		{
			webCamTextureToMatHelper.Pause ();
		}

		/// <summary>
		/// Raises the stop button event.
		/// </summary>
		public void OnStopButton ()
		{
			webCamTextureToMatHelper.Stop ();
		}

		/// <summary>
		/// Raises the change camera button event.
		/// </summary>
		public void OnChangeCameraButton ()
		{
			webCamTextureToMatHelper.Init (null, webCamTextureToMatHelper.requestWidth, webCamTextureToMatHelper.requestHeight, !webCamTextureToMatHelper.requestIsFrontFacing);
		}
	}
}