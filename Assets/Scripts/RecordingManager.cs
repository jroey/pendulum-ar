﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RecordingManager : MonoBehaviour {

	public bool isRecording = false;

	public GameObject baseRef;

	public GameObject indicatorRef;

	public VectorArrow velVecArrow;
	public VectorArrow accVecArrow;
	public VectorArrow mVelVecArrow;
	public VectorArrow mAccVecArrow;

	public Recording recording;

	public int frameIndex = 0;

	public float recordingStart;

	public Text recordingText;

	public static bool isLooping;

	public bool isPaused = false;

	public CameraImageToMatExample camImage;

	public PendulumTracker pendulumTracker;

	public GameObject imageQuad;

	public Slider playbackSlider;

	public MenuManager menuManager;

	public float timeSinceFrame;

	public float playbackSpeed = 1;

	public float playbackSpeedIncrement = 0.2f;

	public float minMaxPlaybackSpeed = 0.6f;

	public UnityEngine.UI.Text playbackSpeedText;

	// Use this for initialization
	void Start () {


		recording = new Recording ();

		recording.frames = new List<Frame> ();

		recordingStart = Time.time;

		recordingText.text = "record";

		imageQuad.SetActive (false);

		menuManager = FindObjectOfType<MenuManager> ();
	}

	public void DecreasePlaybackSpeed(){

		if (playbackSpeed > 1 - minMaxPlaybackSpeed) {
		
			playbackSpeed -= playbackSpeedIncrement;
		
		}

		playbackSpeedText.text = playbackSpeed + "x";

	}

	public void IncreasePlaybackSpeed(){

		if (playbackSpeed < 1 + minMaxPlaybackSpeed) {

			playbackSpeed += playbackSpeedIncrement;

		}

		playbackSpeedText.text = playbackSpeed + "x";

	}
				
	// Update is called once per frame
	void Update () {

		if (isRecording) {

			Frame frame = new Frame ();

			frame.indicatorPos = indicatorRef.transform.position;
			frame.indicatorRot = indicatorRef.transform.rotation;

			frame.basePos = baseRef.transform.position;
			frame.baseRot = baseRef.transform.rotation;	

			frame.cameraPos = Camera.main.transform.position;
			frame.cameraRot = Camera.main.transform.rotation;

			frame.realVelVec = velVecArrow.vector;
			frame.realAccVec = accVecArrow.vector;
			frame.modVelVec = mVelVecArrow.vector;
			frame.modAccVec = mAccVecArrow.vector;

			frame.deltaTime = Time.deltaTime;

			//frame.image = camImage.GetImage ();
			frame.image = pendulumTracker.GetImage ();

			recording.frames.Add (frame);

		} else if (isLooping && !isPaused) {

			timeSinceFrame += Time.deltaTime * playbackSpeed;

			int nextFrameIndex = frameIndex + 1;

			if (nextFrameIndex == recording.frames.Count) {
				nextFrameIndex = 0;
			}

			while (timeSinceFrame >= recording.frames[nextFrameIndex].deltaTime) {
			
				timeSinceFrame -= recording.frames [frameIndex].deltaTime;
			
				frameIndex = nextFrameIndex;

				nextFrameIndex = nextFrameIndex + 1;

				if (nextFrameIndex == recording.frames.Count) {
					nextFrameIndex = 0;
				}

			}

			LoadCurrentFrame ();

			playbackSlider.minValue = 0;
			playbackSlider.maxValue = recording.frames.Count - 1;
			playbackSlider.value = frameIndex;
		}

	}

	public void ScrobbleTo(float value) {
	
		frameIndex = Mathf.FloorToInt(value);
	
		LoadCurrentFrame ();

	}

	private void LoadCurrentFrame () {

		Frame frame = recording.frames [frameIndex];

		indicatorRef.transform.position = frame.indicatorPos;
		indicatorRef.transform.rotation = frame.indicatorRot;

		baseRef.transform.position = frame.basePos;
		baseRef.transform.rotation = frame.baseRot;

		velVecArrow.vector = frame.realVelVec;
		accVecArrow.vector = frame.realAccVec;
		mVelVecArrow.vector = frame.modVelVec;
		mAccVecArrow.vector = frame.modAccVec;

		Camera.main.transform.position = frame.cameraPos;
		Camera.main.transform.rotation = frame.cameraRot;

		///

		Renderer rend = imageQuad.GetComponent<Renderer>();

		// duplicate the original texture and assign to the material
		Texture2D texture = Instantiate(rend.material.mainTexture) as Texture2D;
		rend.material.mainTexture = texture;

		texture.SetPixels (frame.image);

		texture.Apply(false);

	}


	public void ToggleCameraFeed(bool cameraFeed){

		if (isLooping) {
			GameObject.Find ("CameraFeedQuad").GetComponent<MeshRenderer> ().enabled = cameraFeed;
		} else {
			GameObject.Find ("BackgroundPlane").GetComponent<MeshRenderer> ().enabled = cameraFeed;
		}

	}

	public void Return () {

		isLooping = false;
		isPaused = false;
		isRecording = false;
		VuforiaBehaviour.Instance.enabled = true;
		recordingText.text = "record";
		imageQuad.SetActive (false);
		menuManager.ExitPlaybackMode ();

		SceneManager.LoadScene("Menu", LoadSceneMode.Single);

	}
		
	public void Record (){

		if (isLooping) {
			// Do Something

			if (isPaused) {
				isPaused = false;
				recordingText.text = "pause";
			} else {
				isPaused = true;
				recordingText.text = "play";
			}

		} else {
			
			if (isRecording) {

				baseRef.transform.SetParent (null, true);

				recording.duration = Time.time - recordingStart;

				isRecording = false;

				VuforiaBehaviour.Instance.enabled = false;

				recordingText.text = "pause";

				frameIndex = 0;
				timeSinceFrame = 0;

				isLooping = true;

				imageQuad.SetActive (true);
				menuManager.EnterPlaybackMode ();

			} else {

				isRecording = true;

				recording = new Recording ();

				recording.frames = new List<Frame> ();

				recordingStart = Time.time;

				recordingText.text = "stop";
			
			}
		}

	}

}

public class Frame {

	public float deltaTime;

	public Vector3 cameraPos;
	public Quaternion cameraRot;

	public Vector3 indicatorPos;
	public Quaternion indicatorRot;

	public Vector3 basePos;
	public Quaternion baseRot;

	public Vector3 realVelVec;
	public Vector3 realAccVec;
	public Vector3 modVelVec;
	public Vector3 modAccVec;

	public Color[] image;

}

public class Recording {

	public float duration;

	public List<Frame> frames;

}