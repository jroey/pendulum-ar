﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawPendulumString : MonoBehaviour {

	public Transform indicatorTrans;

	public Transform botTrans;

	public VectorArrow arrow;

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void LateUpdate () {

		if (arrow) {

			Vector3 stringVec = (botTrans.position - indicatorTrans.position).normalized * Vector3.Distance(botTrans.position, indicatorTrans.position);

			arrow.vector = stringVec;
			arrow.UpdatePositions ();
		}

	}
}
