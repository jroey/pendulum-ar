﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using Vuforia;
using OpenCVForUnity;

public class PendulumTracker : MonoBehaviour {

	public GameObject penWeightIndicator;

	public GameObject debugQuad;
	public UnityEngine.UI.Text debugText;
	public RectTransform debugIndicator;
	public Transform baseTrans;

	private Image.PIXEL_FORMAT m_PixelFormat;
	private bool m_RegisteredFormat = false;
	public Scalar lowerBound = new Scalar (0, 155, 127, 0);
	public Scalar upperBound = new Scalar (200, 227, 255, 0);

	//public Scalar lowerBound = new Scalar (0, 0, 0, 0);
	//public Scalar upperBound = new Scalar (255, 255, 255, 0);

	Mat inputMat;
	Mat rgbMat;
	Mat hsvMat;
	Mat thresholdMat;
	public Texture2D outputTexture;

	Color32[] colors;

	const int MIN_OBJECT_AREA = 2 * 2;
	const int MAX_NUM_OBJECTS = 50;

	private bool processing = false;

	//private int SCALE_FACTOR = 10;
	private int IMG_SCALE_FACTOR = 1;
	private int PROC_SCALE_FACTOR = 10;

	// Use this for initialization
	void Start () {

		#if UNITY_EDITOR
			m_PixelFormat = Image.PIXEL_FORMAT.RGBA8888;
		#elif UNITY_IOS
			m_PixelFormat = Image.PIXEL_FORMAT.RGB888;
		#endif

		try{
			LoadFromPlayerPrefs ();
		} catch (Exception e) {
			Debug.LogException(e, this);
		}

		VuforiaBehaviour qcarBehaviour = (VuforiaBehaviour)FindObjectOfType (typeof(VuforiaBehaviour));

		if (qcarBehaviour) {
			qcarBehaviour.UpdateEvent += OnTrackablesUpdated;
		}
			

	}

	public void Init(Image image) {

		#if UNITY_EDITOR
			inputMat = new Mat (image.Height, image.Width, CvType.CV_8UC4);
			rgbMat = new Mat (image.Height/PROC_SCALE_FACTOR, image.Width/PROC_SCALE_FACTOR, CvType.CV_8UC3);
		#elif UNITY_IOS
			inputMat = new Mat (image.Height, image.Width, CvType.CV_8UC3);
		#endif

		thresholdMat = new Mat (image.Height, image.Width, CvType.CV_8UC1);
		hsvMat = new Mat ();

		colors = new Color32[inputMat.cols () * inputMat.rows ()];

		Debug.Log ("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);

		if (outputTexture == null) {

			Vector3 debugQuadPos = debugQuad.transform.position;

			#if UNITY_EDITOR
				outputTexture = new Texture2D (inputMat.cols () / IMG_SCALE_FACTOR, inputMat.rows () / IMG_SCALE_FACTOR, TextureFormat.RGBA32, false);
				debugQuadPos.z = outputTexture.width;
			#elif UNITY_IOS
				outputTexture = new Texture2D (inputMat.cols () / PROC_SCALE_FACTOR, inputMat.rows () / PROC_SCALE_FACTOR, TextureFormat.RGB24, false);
				debugQuadPos.z = outputTexture.width;
				debugQuad.transform.Rotate(new Vector3(0,0,-90));
			#endif

			//debugQuad.transform.localScale = new Vector3(outputTexture.width / Screen.width, outputTexture.height / Screen.height, 1);
			//debugQuad.transform.localScale = new Vector3(outputTexture.width, outputTexture.height, 1);

			float camAspectX = (float)outputTexture.width/(float)outputTexture.height;

			Camera cam = Camera.main;

			float pos = (cam.nearClipPlane + 0.01f)+2;

			debugQuad.transform.position = cam.transform.position + cam.transform.forward * pos;

			float h = Mathf.Tan(cam.fieldOfView*Mathf.Deg2Rad*0.5f)*pos*2f;

			debugQuad.transform.localScale = new Vector3(h*camAspectX,h,0f);

			#if UNITY_IOS
				//float w = Mathf.Tan(cam.fieldOfView*Mathf.Deg2Rad*0.5f)*pos*2f;
				//debugQuad.transform.localScale = new Vector3(w,h*camAspectX,0f);

				debugQuad.transform.localScale = new Vector3(debugQuad.transform.localScale.x/1.75f, debugQuad.transform.localScale.y/1.75f,0);
			#endif



			//debugQuad.transform.position = debugQuadPos;

		}

		debugQuad.GetComponent<Renderer> ().material.mainTexture = outputTexture;

	}

	int ticker = 0;

	IEnumerator UpdateTracking()
	{

		if (!m_RegisteredFormat) {
			CameraDevice.Instance.SetFrameFormat (m_PixelFormat, true);
			m_RegisteredFormat = true;
		}

		CameraDevice cam = CameraDevice.Instance;

		Image image = cam.GetCameraImage (m_PixelFormat);

		if (image == null) {
			Debug.Log (m_PixelFormat + " image is not available yet");
			debugText.text = m_PixelFormat + " image is not available yet";
		} else {

			debugText.text = m_PixelFormat + " ready";

			if (inputMat == null) {

				Init (image);

			}


			inputMat.put (0, 0, image.Pixels);

			//yield return null;

			#if UNITY_EDITOR
				Imgproc.cvtColor (inputMat, rgbMat, Imgproc.COLOR_RGBA2RGB);
				Imgproc.cvtColor (rgbMat, hsvMat, Imgproc.COLOR_RGB2HSV);
			#elif UNITY_IOS
				Imgproc.cvtColor (inputMat, hsvMat, Imgproc.COLOR_RGB2HSV);
			#endif

			//yield return null;

			Core.inRange (hsvMat, lowerBound, upperBound, thresholdMat);

			//yield return null;

			//Utils.matToTexture2D (thresholdMat, outputTexture, colors);

			trackPendulum (thresholdMat);

		}

		processing = false;

		yield return null;

	}

	public Color[] GetImage(){
		return outputTexture.GetPixels();
	}

	public void OnTrackablesUpdated ()
	{

		float timee = Time.realtimeSinceStartup;

		if (!m_RegisteredFormat) {
			CameraDevice.Instance.SetFrameFormat (m_PixelFormat, true);
			m_RegisteredFormat = true;
		}

		CameraDevice cam = CameraDevice.Instance;

		Image image = cam.GetCameraImage (m_PixelFormat);

		if (image == null) {
			Debug.Log (m_PixelFormat + " image is not available yet");
			debugText.text = m_PixelFormat + " image is not available yet";
		} else {

			debugText.text = m_PixelFormat + " ready";

			if (inputMat == null) {

				Init (image);

			}

			inputMat.put (0, 0, image.Pixels);

			Mat inn = new Mat (inputMat.height()/PROC_SCALE_FACTOR, inputMat.width()/PROC_SCALE_FACTOR, CvType.CV_8UC4);

			Imgproc.resize (inputMat, inn, new Size (inputMat.width() / PROC_SCALE_FACTOR, inputMat.height() / PROC_SCALE_FACTOR));

			#if UNITY_EDITOR
				Imgproc.cvtColor (inn, rgbMat, Imgproc.COLOR_RGBA2RGB);

				Utils.matToTexture2D (inputMat, outputTexture, colors);

				Imgproc.cvtColor (rgbMat, hsvMat, Imgproc.COLOR_RGB2HSV);
			#elif UNITY_IOS
				Utils.matToTexture2D (inn, outputTexture, colors);
				Imgproc.cvtColor (inn, hsvMat, Imgproc.COLOR_RGB2HSV);
			#endif

			Core.inRange (hsvMat, lowerBound, upperBound, thresholdMat);

			//Utils.matToTexture2D (thresholdMat, outputTexture, colors);

			trackPendulum (thresholdMat);

		}



		debugText.text = (Time.realtimeSinceStartup - timee).ToString();

		if (processing) {
			return;
		}

		processing = true;


		//StartCoroutine(UpdateTracking());

		/*ticker++;

		if (ticker % 3 != 0) {
		
			return;
		
		}*/


	}

	void trackPendulum(Mat threshold){

		//Mat temp = new Mat ();
		//threshold.copyTo (temp);

		List<MatOfPoint> contours = new List<MatOfPoint> ();

		Mat hierarchy = new Mat ();

		//find contours of filtered image using openCV findContours function
		Imgproc.findContours (threshold, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

		int numObjects = hierarchy.rows ();

		if (numObjects > 0) {

			if (numObjects < MAX_NUM_OBJECTS) {

				double maxArea = 0;
				double xPos = 0;
				double yPos = 0;

				for (int index = 0; index >= 0; index = (int)hierarchy.get (0, index) [0]) {

					Moments moment = Imgproc.moments (contours [index]);
					double area = moment.get_m00 ();

					if (area > MIN_OBJECT_AREA && area > maxArea) {

						maxArea = area;

						xPos = moment.get_m10 () / area;
						yPos = moment.get_m01 () / area;

					}

				}

				if (maxArea > 0) {
					//debugText.text = xPos + " | " + yPos;

					/*#if UNITY_EDITOR
					xPos = (int)(moment.get_m10 () / area);
					yPos = (int)(moment.get_m01 () / area);
					#elif UNITY_IOS
					xPos = (int)(moment.get_m01 () / area);
					yPos = (int)(moment.get_m10 () / area);
					#endif*/

					#if UNITY_EDITOR
						debugText.text = new Vector2(((float)xPos/threshold.width()) * Screen.width, ((float)yPos/threshold.height()) * Screen.height).ToString();
						debugIndicator.anchoredPosition = new Vector2 (((float)xPos / threshold.width ()) * Screen.width, (1-((float)yPos / threshold.height ())) * Screen.height);
					#elif UNITY_IOS
						debugText.text = new Vector2((1-((float)yPos/threshold.height())) * Screen.width, ((float)xPos/threshold.width()) * Screen.height).ToString();
						debugIndicator.anchoredPosition = new Vector2((1-((float)yPos/threshold.height())) * Screen.width, (1-((float)xPos/threshold.width())) * Screen.height);
					#endif

					Ray ray = Camera.main.ScreenPointToRay(debugIndicator.anchoredPosition);

					Debug.DrawRay(Camera.main.transform.position, ray.direction * 1000, Color.yellow);

					RaycastHit hit;

					if (Physics.Raycast (ray, out hit, 1000f, 1<<8)) {

						penWeightIndicator.transform.position = hit.point;
					} 


				}

			} else {

				debugText.text = "Filter too noisey";

			}

		}

	}

	void LoadFromPlayerPrefs(){

		lowerBound.val [0] = (double)PlayerPrefs.GetFloat ("L0");
		lowerBound.val [1] = (double)PlayerPrefs.GetFloat ("L1");
		lowerBound.val [2] = (double)PlayerPrefs.GetFloat ("L2");
		lowerBound.val [3] = (double)PlayerPrefs.GetFloat ("L3");

		upperBound.val [0] = (double)PlayerPrefs.GetFloat ("U0");
		upperBound.val [1] = (double)PlayerPrefs.GetFloat ("U1");
		upperBound.val [2] = (double)PlayerPrefs.GetFloat ("U2");
		upperBound.val [3] = (double)PlayerPrefs.GetFloat ("U3");

		Debug.Log ("Values loaded from prefs");

	}
}
