﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCVForUnitySample;

public class DisplayFilterValues : MonoBehaviour {

	public WebCamTextureDetectPendulum pendulumDetector;

	private TextMesh textMesh;

	// Use this for initialization
	void Start () {

		textMesh = GetComponent<TextMesh> ();

	}
	
	// Update is called once per frame
	void Update () {

		textMesh.text = pendulumDetector.orangeLower + " \n " + pendulumDetector.orangeUpper;

	}
}
