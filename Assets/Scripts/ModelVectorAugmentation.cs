﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelVectorAugmentation : MonoBehaviour {

	public PendulumProperties properties;

	public VectorArrow modelVelArrow;
	public VectorArrow modelAccArrow;

	public Transform baseTransform;
	public Transform bobTransform;

	public Transform randTrans;

	// Use this for initialization
	void Start () {


	}

	float GetThetaFromMax(){

		float returnVal = 0;

		if (properties.maxAngle != 0)
			returnVal = (Mathf.PI / 2) * (properties.angle / properties.maxAngle);

		return returnVal;

	}

	float GetThetaFromMax(float angle){

		float returnVal = 0;

		if (properties.maxAngle != 0)
			returnVal = (Mathf.PI / 2) * (angle / properties.maxAngle);

		return returnVal;

	}
	
	// Update is called once per frame
	void Update () {

		if (RecordingManager.isLooping) {
			return;
		}

		float maxAngle = properties.maxAngle;

		float length = properties.length;

		float direction = properties.xDir;

		//modelVelArrow.vector = new Vector3 (direction, 0, 0);

		//modelVelArrow.vector = (baseTransform.position - bobTransform.position).normalized;

		Vector3 modelVelVec = Vector3.Cross (Vector3.forward * -properties.xDir * Mathf.Cos(GetThetaFromMax()), (baseTransform.position - bobTransform.position).normalized);

		modelVelArrow.vector = modelVelVec;

		float side = 1;

		if (bobTransform.position.x < 0)
			side = -1;

		float prevAngle;

		if ((bobTransform.position.x < 0 && properties.xDir > 0) || (bobTransform.position.x > 0 && properties.xDir < 0)) {
			// angle decreasing

			prevAngle = (properties.angle + 3f) * Mathf.Deg2Rad;

		} else {
			// angle increasing

			prevAngle = (properties.angle - 3f) * Mathf.Deg2Rad;

		}

		//float distance = properties.length;
		float distance = Vector3.Distance (baseTransform.position, bobTransform.position);

		Vector3 prevPos = baseTransform.position;
		prevPos.x += distance * Mathf.Sin (prevAngle * side);
		prevPos.y -= distance * Mathf.Cos (prevAngle * side);

		Vector3 prevVelVec = Vector3.Cross (Vector3.forward * -properties.xDir * Mathf.Cos(GetThetaFromMax(prevAngle * Mathf.Rad2Deg)), (baseTransform.position - prevPos).normalized);

		Vector3 modelAccVec = (modelVelVec - prevVelVec).normalized;

		//Vector3 modelAccVec = (modelVelVec - prevVelVec).normalized;

		//randTrans.position = prevPos;

		modelAccArrow.vector = modelAccVec;

		Camera camera = Camera.main;
		//Vector3 p = camera.ViewportToWorldPoint(new Vector3(1, 1, camera.nearClipPlane));

		//randTrans.position = p;


	}
}
