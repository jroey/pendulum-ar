﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableGameobject : MonoBehaviour {

	public GameObject gObject;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Enable(bool isEnabled){

		gObject.SetActive (isEnabled);

	}
}
