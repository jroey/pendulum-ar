﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtBase : MonoBehaviour {

	public Transform topTrans;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.LookAt (topTrans.position);

	}
}
