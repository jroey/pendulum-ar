﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawWave : MonoBehaviour {

	public Transform indicatorTrans;  

	private LineRenderer lineRenderer;

	public float frequency;

	private float prevTime;

	private int lastIndex = 1;

	public float moveSpeed;

	private bool sinWaveEnabled = false;

	// Use this for initialization
	void Start () {

		lineRenderer = GetComponent<LineRenderer> ();

		lineRenderer.useWorldSpace = false;
		lineRenderer.SetPosition (0, Vector3.zero);

		lineRenderer.SetPosition (0, indicatorTrans.position);
	}
	
	// Update is called once per frame
	void Update () {

		transform.Translate (-transform.up * moveSpeed * Time.deltaTime);

		if (lastIndex < lineRenderer.positionCount - 1 && Time.time - prevTime >= frequency) {

			prevTime = Time.time;

			lastIndex += 1;

			for (int i = 0; i < lineRenderer.positionCount; i++) {
			
				if (i < lastIndex) {
					lineRenderer.SetPosition (i, lineRenderer.GetPosition (i + 1));
				}
				else{
					lineRenderer.SetPosition (i, indicatorTrans.position-transform.position);
				}

			}
		} else if (lastIndex >= lineRenderer.positionCount) {

			lastIndex = 0;

		}
	
		lineRenderer.enabled = sinWaveEnabled;

	}

	public void ToggleSinWave (bool sinWaveEnabled){
		this.sinWaveEnabled = sinWaveEnabled;
	}
}
