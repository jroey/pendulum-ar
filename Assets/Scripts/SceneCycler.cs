﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneCycler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		for (int i = 0; i < Input.touchCount; ++i)
		{
			if (Input.GetTouch (i).phase == TouchPhase.Began) {
				Debug.Log ("touch began");

				SwapScenes ();
			}
		}

		/*
		if (Input.GetMouseButtonDown (0)) {
			SwapScenes ();
		}*/
	}

	public void SwapScenes(){

		int nextSceneIndex = SceneManager.GetActiveScene ().buildIndex + 1;

		if (nextSceneIndex < SceneManager.sceneCountInBuildSettings) {
			SceneManager.LoadScene (nextSceneIndex);
		} else {
			SceneManager.LoadScene (0);
		}
	}
}
