﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumProperties : MonoBehaviour {

	public Transform baseTrans;

	public GameObject penWeightIndicator;

	public float angle; // degrees
	public float length; // cm
	public float mass; // grams
	public float period; // seconds

	public TextMesh angleText;
	public TextMesh lengthText;

	public float maxAngle;

	public float maxAngleResetTime = 10f;

	public float nextMaxAngle;

	public float xDir;

	private float prevX;

	//private float HEIGHT_SCALAR = 1.5f;

	private float HEIGHT_SCALAR = 1f;

	// Use this for initialization
	void Start () {

		InvokeRepeating("CheckAngle", 0, maxAngleResetTime);
	
	}

	void CheckAngle() {
		
		maxAngle = nextMaxAngle;
		nextMaxAngle = 0;

	}
	
	// Update is called once per frame
	void Update () {

		getPendulumProperties ();

	}

	void getPendulumProperties (){

		if (Mathf.Abs (Mathf.RoundToInt (angle)) > nextMaxAngle) {

			nextMaxAngle = Mathf.Abs(Mathf.RoundToInt (angle));

			if (nextMaxAngle > maxAngle)
				maxAngle = nextMaxAngle;

		}

		Vector3 targetDir = baseTrans.position - penWeightIndicator.transform.position;

		angle = Vector3.Angle(targetDir, baseTrans.forward);

		length = Vector3.Distance (baseTrans.position, penWeightIndicator.transform.position) * 100 * HEIGHT_SCALAR;

		angleText.text = Mathf.RoundToInt(angle) + " °";

		lengthText.text = Mathf.RoundToInt(length) + " cm";

		float xPos = penWeightIndicator.transform.position.x;

		if (xPos > prevX) {
			xDir = 1;
		} else if (xPos < prevX) {
			xDir = -1;
		} else {
			//xDir = 0;
		}

		prevX = xPos;

	}
}
