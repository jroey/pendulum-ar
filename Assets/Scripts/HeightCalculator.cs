﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class HeightCalculator : MonoBehaviour {

	public bool topReady;
	public bool bottomReady;

	public Transform topTransform;
	public Transform botTransform;

	public GameObject topIndicator;
	public GameObject botIndicator;

	private int numSampledFrames = 0;
	public static float height = 0;

	public float currheight;

	public int frameSampleThreshold;

	public Transform baseTransform;

	private bool isTrackingMarker(string imageTargetName)
	{
		var imageTarget = GameObject.Find(imageTargetName);
		var trackable = imageTarget.GetComponent<TrackableBehaviour>();
		var status = trackable.CurrentStatus;
		return status == TrackableBehaviour.Status.TRACKED;
	}

	// Use this for initialization
	void Start () {
		
	}
		
	// Update is called once per frame
	void Update () {

		topReady = isTrackingMarker ("Astronaut");
		bottomReady = isTrackingMarker ("Drone");

		topIndicator.SetActive (topReady);
		botIndicator.SetActive (bottomReady);

		if (topReady && bottomReady && numSampledFrames < frameSampleThreshold) {

			float distance = Vector3.Distance (topTransform.position, botTransform.position);

			numSampledFrames++;

			currheight = distance;

			height = (height + distance) / numSampledFrames;

			if (numSampledFrames == frameSampleThreshold) {

				baseTransform.gameObject.SetActive (true);

				Vector3 basePos = baseTransform.position;

				height = distance;

				basePos = basePos + baseTransform.forward * height;

				baseTransform.position = basePos;

				gameObject.SetActive (false);

			}

		} else if (numSampledFrames >= frameSampleThreshold){

			topIndicator.SetActive (false);
			botIndicator.SetActive (false);

		}

	}
}
