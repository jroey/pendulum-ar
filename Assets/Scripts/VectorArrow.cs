﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorArrow : MonoBehaviour {

	public Transform bodyTransform;

	public Transform headTransform;

	public Vector3 vector;

	public float scaler = 0.5f;

	public float minMagnitude = 0.2f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		UpdatePositions ();

	}

	public void UpdatePositions(){

		Vector3 scaledVector = vector * scaler;

		Vector3 relativePos = scaledVector;

		bodyTransform.rotation = Quaternion.LookRotation(scaledVector);
		bodyTransform.localScale = new Vector3 (1, 1, minMagnitude + scaledVector.magnitude);

		headTransform.position = transform.position + scaledVector + (scaledVector.normalized * minMagnitude);
		headTransform.rotation = Quaternion.LookRotation(scaledVector);

	}

}
