﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickerTape : MonoBehaviour {

	public GameObject indicator;

	public GameObject tickPrefab;

	public static float frequency = 0.03f;

	private static float lastTime;

	public List<Vector3> positions;

	public float tickScale;

	public bool isMenu = false;

	// Use this for initialization
	void Start () {

		if (isMenu) {

			frequency = 0.1f;

		} else {

			frequency = 0.03f;

		}

	}
	
	// Update is called once per frame
	void Update () {

		if (Time.time - lastTime >= frequency) { 

			positions.Add (indicator.transform.position);

			GameObject tick = Instantiate (tickPrefab, indicator.transform.position, Quaternion.identity);

			if (tickScale != 0) {

				tick.transform.localScale = new Vector3 (tickScale, tickScale, tickScale);

			}

			lastTime = Time.time;

		}

	}

	void CreateSphere() {

	}

	public static void DirectionReversed () {

		lastTime = frequency;

		DestroyTicks ();

	}

	public static void DestroyTicks() {

		GameObject[] ticks = GameObject.FindGameObjectsWithTag ("Tick");

		for (int i = 0; i < ticks.Length; i++) {

			Destroy (ticks [i]);

		}
	}

	void OnDisable()
	{
		DestroyTicks ();
	}

	void OnEnable()
	{
		DestroyTicks ();
	}

}
