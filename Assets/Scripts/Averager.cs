﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Averager : MonoBehaviour {

	public float average = 0;

	private int numFrames = 0;

	private bool isAveraging = false;

	public PendulumProperties pendulumProperties;

	private float averageTime = 0;

	public Text buttonText;

	public Text dispText;

	public AverageVariable targetVar;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (isAveraging) {

			float value = 0;

			if (targetVar == AverageVariable.Height) {

				value = pendulumProperties.length;

			}

			numFrames++;
			average += value;
			averageTime += Time.deltaTime;
		}

	}

	public void Average() {

		if (isAveraging) {
			StopAveraging ();
		} else {
			StartAveraging ();
		}

	}

	public void StartAveraging() {

		buttonText.text = "stop";

		average = 0;
		numFrames = 0;
		averageTime = 0;

		isAveraging = true;

	}

	public void StopAveraging() {
		
		buttonText.text = "average";

		isAveraging = false;

		average = average / numFrames;

		dispText.text = "Average: " + average + " in " + averageTime + " seconds";

	}

}

public enum AverageVariable {

	Angle,
	Height,
	DiffVel,
	DiffAcc

}
