﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumSimulator : MonoBehaviour {

	public float speed = 1;
	public float horAmplitude;
	public float verAmplitude;

	float initialY;
	float initialX;

	public float angle = 45;
	float vel = 0;
	float acc = 0;

	public float length = 0.1f;

	public float g;

	public Transform baseTrans;

	float avel;

	public PendulumProperties properties;

	// Use this for initialization
	void Start () {

		initialY = baseTrans.transform.position.y;
		initialX = baseTrans.transform.position.x;

		angle = Mathf.Deg2Rad * angle;

	}

	float frameIndex = 0;

	float resetFrames = 100;

	// Update is called once per frame
	void Update () {

		if (properties) {

			//length = HeightCalculator.height;

			frameIndex++;

			if (frameIndex % resetFrames == 0) {
				
				angle = properties.maxAngle * Mathf.Deg2Rad;
				acc = 0;
				vel = 0;
				avel = 0;
			}

		}

		initialY = baseTrans.transform.position.y;
		initialX = baseTrans.transform.position.x;

		Vector3 pos = Vector3.zero;

		pos.z = baseTrans.position.z;

		//pos.x = initialX + Mathf.Sin (Time.time * speed) * horAmplitude;
		//pos.y = initialY - Mathf.Abs(Mathf.Cos (Time.time * speed) * verAmplitude);

		acc = ((-g)/length) * Mathf.Sin (angle);

		avel += acc;
		angle += avel;

		pos.x = initialX + (length * Mathf.Sin (angle));
		pos.y = initialY - (length * Mathf.Cos (angle));

		transform.position = pos;

	}
}
